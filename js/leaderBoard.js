/**
 * Created by sametatabasch on 14.06.2016.
 *
 * Bu dosya ile projenin veri saklama işlemleri düzenlenecek.
 * anlaşılmasının daha kolay olması için farklı bir js dosyası kullanmayı tercihettim.
 *
 * Veri saklamak için HTML5 localstorage kullanılacak
 */

var leaderBoard = function () {
    var _this = this;
    this.debug = false;
    /**
     * geme nesnesi
     *
     * @type {game}
     */
    this.parent = {};
    /**
     * lider tablosunu dizi tipinde saklar
     * @type {Array}
     */
    this.leaders = [];
    /**
     * Lider tablosu eleman sayısını saklar
     * @type {number}
     */
    this._lenght = 0;
    /**
     * Lider tablosuna eklenecek olan yeni oyuncu verilerini saklar
     * @type {{name: string, score: number}}
     */
    this.newPlayer = {name: '', score: 0};
    /**
     * Lider tablosundaki minumum skoru saklar
     * @type {number}
     */
    this.minScore = 0;
    /**
     * liderler tablosunda saklanacak maksimum sonuç sayısı
     * @type {number}
     */
    this.limit = 10;
    /**
     * Yeni eklenen liderin eklendiği sıra numarası
     * @type {number}
     */
    this.currentGameRank = 0;
    /**
     * Sınıfı kullanmadan önce güncel verilerle işlem yapmak için bilgilsei yeniler
     */
    this.refresh = function () {
        if(_this.debug) console.log('leaderBorad refresh()');
        if (_this.get()) {
            if(_this.debug) console.log('leaderBoard refresh() lb.get() true');
            // set min score
            _this.minScore = _this.leaders[_this._lenght - 1].score;
        } else {
            if(_this.debug) console.log('leaderBoard refresh() lb.get() false');
            _this.minScore = 0;
            _this._lenght = 0;
        }
    };

    /**
     * Localstorage de tutulan liter tablosunu getirir
     * @returns {boolean,Array}
     */
    this.get = function () {
        if(_this.debug) console.log('leaderBoard get()');
        try {

            if (!localStorage.leaderBoard) {
                return false;

            } else {
                var leaderBoardString = '{"LB":[' + localStorage.leaderBoard + ']}';
                _this.leaders = JSON.parse(leaderBoardString).LB;
                _this._lenght = _this.leaders.length;
                // score değerlerine göre sıralanıyor
                _this.leaders.sort(function (a, b) {
                    if (a.score < b.score)return 1;
                    if (a.score > b.score)return -1;
                    if (a.score == b.score)return 0;
                });
                return _this._lenght != 0 ? _this.leaders : false;
            }
        } catch (e) {
            console.error(e);
            return false;
        }
    };
    /**
     * Parametre olarak gelen isim ve skoru localstorege e kaydeder
     * @param name
     * @param score
     * @param level
     * @returns {boolean}
     */
    this.set = function (name, score, level) {
        if(_this.debug) console.log('leaderBoard lb.set()');
        try {
            _this.refresh();
            // bir sonraki oyun sonunda kullanıcıya kolaylık olması için en son yazılmış ismi otomatik olarak inputa yazmak için saklıyoruz.
            localStorage.previousName = name;
            _this.newPlayer.name = name + ' (x' + level + ')';
            _this.newPlayer.score = score;
            /**
             * Eğer listede veri var ise yeni veri listede uygun yere eklenecek yok ise listeye ilk veri eklenecek
             */
            if (_this._lenght > 0) {
                /*
                 * Yeni skokun eski skorla değiştirilip değiştirilmediğini tutar
                 */
                var isChanged = false;
                var tmp, tmp1;
                _this.leaders.forEach(function (leader, counter) {
                    /**
                     * yeni veri eklendikten sonra eski veriler bir sıra kaydırılıyor
                     */
                    if (isChanged) {
                        // şuanki veri yedekleniyor
                        tmp1 = leader;
                        // şuanki veri bir önceki döngüden gelen değerile değiştiriliyor
                        _this.leaders[counter] = tmp;
                        // alınan yedek veri bir sonreki döngüde kullanılmak üzere tmp değişkenine atanıyor
                        tmp = tmp1;
                        if (counter + 1 == _this._lenght && _this._lenght <= _this.limit) {
                            _this.leaders[counter + 1] = tmp;
                        }

                    }
                    /**
                     * Yeni veri listede kendisinden düşük yada eşit olan ilk veri yerine eklenecek
                     * eğer listede kendisinden küçük veri yok ilse listeye eklememiş olacak
                     */
                    if (leader.score <= _this.newPlayer.score && !isChanged) {
                        // şuanki veri yedekleniyor ve bir sonraki döngüde kullanılacak
                        tmp = leader;
                        // veri yenisi ile değiştiriliyor
                        _this.leaders[counter] = _this.newPlayer;
                        // yeni oyun sonucunun liderler listesine eklendiği belirtiliyor
                        isChanged = true;
                        //limit dolmamış ve ve son elemana gelinmişse liste sonuna son kalan veri eklenir.
                        if (counter == _this._lenght - 1 && _this._lenght + 1 <= _this.limit) {
                            _this.leaders[counter + 1] = tmp;// yedeğe alınan değer bir sonraki sütüna ekleniyor
                            //Eklenen oyuncunun listedeki sırası
                            _this.currentGameRank = counter + 1;
                        } else
                        //Eklenen oyuncunun listedeki sırası
                            _this.currentGameRank = counter + 1;
                    } else if (counter + 1 == _this._lenght && !isChanged) {
                        //limit dolmamış ve alınan puandan küçük hiç puan yoksa liste sonuna alınan puan eklenir
                        _this.leaders[counter + 1] = _this.newPlayer;
                        //Eklenen oyuncunun listedeki sırası
                        _this.currentGameRank = counter + 2;
                    }
                });
                //JSON.stringify çıktısındaki "[" ve "]" sembollerini çıkartıp localstogare e kaydediyoruz
                localStorage.leaderBoard = JSON.stringify(_this.leaders).slice(1, -1);
                return true;
            } else {
                localStorage.leaderBoard = '{"name":"' + name + ' (x' + level + ')","score":"' + score + '" }';
                _this.currentGameRank = 1;
                return true;
            }
        } catch (e) {
            console.error(e);
        }
    };

    this.show = function (limit) {
        if(_this.debug) console.log('leaderBoard show()');
        try {
            limit = typeof limit === 'undefined' ? _this.limit : limit;
            _this.refresh();
            if (_this._lenght == 0)
                return '<div class="card">' +
                    '       <div class="card-content">' +
                    '           <div class="card-content-inner"><p class="align-center">Henüz hiç skor kaydedilmemiş.</p></div>' +
                    '       </div>' +
                    '</div> ';
            var out =
                '<div class="list-block">' +
                '   <ul>';
            _this.leaders.forEach(function (leader, counter) {
                var rank = (counter + 1) + '.';

                if (rank <= limit) {
                    if (rank == '1.') rank = '1. <img src="img/gold.png" width="32px">';
                    if (rank == '2.') rank = '2. <img src="img/bronze.png" width="32px">';
                    if (rank == '3.') rank = '3. <img src="img/silver.png" width="32px">';
                    out +=
                        '       <li class="item-content">' +
                        '           <div class="item-media">' + rank + ' </div>' +
                        '           <div class="item-inner">' +
                        '               <div class="item-title">' + leader.name + '</div>' +
                        '               <div class="item-after">' + leader.score + '</div>' +
                        '           </div>' +
                        '       </li>';
                }
            });

            out +=
                '   </ul>' +
                '</div>';
            return out;
        } catch (e) {
            console.error(e);
            return false;
        }
    };

    this.showGameScore = function () {
        if(_this.debug) console.log('leaderBoard showGameScore()');
        try {
            _this.refresh();
            if (_this._lenght == 0)
                return '<div class="card">' +
                    '       <div class="card-content">' +
                    '           <div class="card-content-inner"><p class="align-center">Henüz hiç skor kaydedilmemiş.</p></div>' +
                    '       </div>' +
                    '</div> ';
            var limitMin = _this.currentGameRank <= 3 ? 1 : _this.currentGameRank - 2;
            var limitMax = _this.currentGameRank >= _this.limit - 2 ? _this.limit : _this.currentGameRank + 2;
            var out =
                '<div class="list-block">' +
                '   <ul>';
            _this.leaders.forEach(function (leader, counter) {
                var rank = counter + 1;
                if (rank >= limitMin && rank <= limitMax) {
                    if (rank == _this.currentGameRank) rank += '. <i class="material-icons">forward</i>';
                    else rank += '.';

                    out +=
                        '       <li class="item-content">' +
                        '           <div class="item-media">' + rank + ' </div>' +
                        '           <div class="item-inner">' +
                        '               <div class="item-title">' + leader.name + '</div>' +
                        '               <div class="item-after">' + leader.score + '</div>' +
                        '           </div>' +
                        '       </li>';
                }
            });
            out +=
                '   </ul>' +
                '</div>';
            return out;
        } catch (e) {
            console.error(e);
        }
    };

    this._reset = function () {
        if(_this.debug) console.log('leaderBoard _reset()');
        localStorage.removeItem('leaderBoard');
    };
    /**
     * Call this.refresh()
     */
    _this.refresh();
};