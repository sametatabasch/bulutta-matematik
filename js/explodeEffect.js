/**
 * Created by sametatabasch on 06.08.2016.
 * @link http://www.inserthtml.com/2013/08/exploding-blocks-css-and-javascript/
 */
var explodeEffect = function () {
    var _this = this;
    this.debug = false;
    this.xAmount = 5;
    this.yAmount = 8;

    // A variable check for when the animation is mostly over
    this.first = false;
    /**
     * patlama efekti için kullanılacak nesne parçalarını oluşturur
     */
    this.clip = function () {
        if (_this.debug) console.log('explodeEffect clip');
        // For easy use
        $t = $$('.balloon');

        // Get the width of each clipped rectangle.
        var width = Math.round($t.width() / _this.xAmount);
        var height = Math.round($t.height() / _this.yAmount);

        // The total is the square of the amount
        var totalSquares = _this.xAmount * _this.yAmount;
        $t.each(function () {
            // The HTML of the content
            var html = $$(this).find('.content').html();

            var y = 0;

            for (var z = 0; z <= (_this.xAmount * width); z = z + width) {

                $$('<div class="clipped" style="clip: rect(' + y + 'px, ' + (z + width) + 'px, ' + (y + height) + 'px, ' + z + 'px)">' + html + '</div>').appendTo($$(this));

                if (z === (_this.xAmount * width) - width) {

                    y = y + height;
                    z = -width;

                }

                if (y === (_this.yAmount * height)) {
                    z = 9999999;
                }

            }
        });


    };
    /**
     * efeket içierisinde kullanılacak rasgele sayı üretim işlemlerini yapar
     * @param min
     * @param max
     * @returns {*}
     */
    this.rand = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    /**
     *  patlama efektini uygulan
     * @param ths tıklalan nesneye erişmek için kullanılan değişken
     * @param isCorrect Cevabın doğru cevap olup olmadığını belirler
     */
    this.explode = function (ths, isCorrect) {
        if (_this.debug) console.log('explodeEffect explode');
        /**
         * tıklanan balon nesnesi
         */
        var b = $$(ths);

        $$(b).find('.content').hide();
        // Apply to each clipped-box div.
        $$(b).find('div:not(.content)').each(function () {
            // So the speed is a random speed between 90m/s and 120m/s. I know that seems like a lot
            // But otherwise it seems too slow. That's due to how I handled the timeout.
            var v = _this.rand(90, 60),
                angle = _this.rand(80, 89), // The angle (the angle of projection) is a random number between 80 and 89 degrees.
                theta = (angle * Math.PI) / 180, // Theta is the angle in radians
                g = -12; // And gravity is -9.8. If you live on another planet feel free to change

            // $(this) as self
            var self = $$(this);

            // time is initially zero, also set some random variables. It's higher than the total time for the projectile motion
            // because we want the squares to go off screen.
            var t = 0,
                z, r, nx, ny,
                totalt = Math.sqrt((($$('.balloon-row').offset().top)*2/g*(-1)));// @link https://en.wikipedia.org/wiki/Free_fall (Example bölümündeki grafik)

            // The direction can either be left (1), right (-1) or center (0). This is the horizontal direction.
            var negate = [1, -1, 0],
                direction = negate[Math.floor(Math.random() * negate.length)];

            // Set an interval
            z = setInterval(function () {

                // Horizontal speed is constant (no wind resistance on the internet)
                var ux = ( Math.cos(theta) * v ) * direction;

                // Vertical speed decreases as time increases before reaching 0 at its peak
                var uy = ( Math.sin(theta) * v ) - ( (-g) * t);

                // The horizontal position
                nx = (ux * t);

                // s = ut + 0.5at^2
                ny = (uy * t) + (0.5 * (g) * Math.pow(t, 2));

                // Apply the positions
                $$(self).css({'bottom': (ny) + 'px', 'left': (nx) + 'px'});

                // Increase the time by 0.10
                t = t + 0.10;

                // If the time is greater than the total time clear the interval
                if (t > totalt) {

                    _this.first = true;
                    /**
                     * patlama efekti sona erdiğinde tetiklenecek olay
                     * @type {Event}
                     */
                    var afterExplodeEffectEventTrue = new Event('afterExplodeEffectTrue');
                    var afterExplodeEffectEventFalse = new Event('afterExplodeEffectFalse');

                    // patlama efeti sorasında olaylar tetikleniyor.
                    // patlama efeti sorasında olaylar tetikleniyor.
                    if (isCorrect) {
                        document.dispatchEvent(afterExplodeEffectEventTrue);
                        if (_this.debug) console.log('afterExplodeEffectEventTrue tetiklendi');
                    } else {
                        document.dispatchEvent(afterExplodeEffectEventFalse);
                        if (_this.debug) console.log('afterExplodeEffectEventFalse tetiklendi');
                    }
                    // tıkalan balon siliniyor.
                    b.remove();
                    // Finally clear the interval
                    clearInterval(z);

                }

            }, 10); // Run this interval every 10ms. Changing this will change the pace of the animation

        });
    };
};