/**
 * Created by sametatabasch on 04.10.2015.
 */
var timer = function () {
    'use strict';
    var _this = this;
    this.debug = false;
    this.parent = {};
    /**
     * timer bir saniyenin herhangi bir yerinde durdurulduğunda örneğin 800. mili saniyede. Tekrar timer çalıştırıldığında bu 80 mili saniye
     * yok sayılıp aynı saniye baştan başlamış oluyor. Bunun önüne geçmek için timer 250 milisaniyede bir çalışacak şekilde çalıştırılması için
     * kaçıncı çalışma içerisinde olduğunu tutan değişken.
     * @type {boolean}
     */
    this.tiktak = 0;

    this.set = function (param) {
        param = typeof param === 'undefined' ? {} : param;
        var Minute = param.minute || 1;
        var Second = param.second || 0;
        _this.domElementId = param.DomElementId || 'showTimer';
        _this.domElement = document.getElementById(_this.domElementId);
        _this.callback = param.callback || function () {
            };

        if(_this.debug) console.log('timer.set (' + Minute + ',' + Second + ')');
        // Eğer var olan bir sayaç devam ediyorsa yeni verileri işleme
        if (_this.minute > 0 || _this.second > 0) return;
        _this.second = typeof Second !== 'undefined' ? Second : 0;
        _this.minute = typeof Minute !== 'undefined' ? Minute : 0;
    };

    this.createText = function () {
        if(_this.debug) console.log('timer.createText');
        var sec = _this.second < 10 ? '0' + _this.second.toString() : _this.second.toString();
        var min = _this.minute < 10 ? '0' + _this.minute.toString() : _this.minute.toString();

        return min + ':' + sec;
    };

    this.start = function () {
        if(_this.debug) console.log('timer.start');
        if (typeof _this.second !== 'undefined' && _this.minute !== 'undefined' && typeof _this.Interval === 'undefined') {
            _this.domElement.innerHTML = _this.createText();
            _this.Interval = setInterval(_this.run, 250);
        }
    };

    this.run = function () {
        if(_this.debug) console.log('timer.run');
        if (_this.tiktak % 4 === 0) {
            _this.second--;
            if (_this.second < 0) {

                if (_this.minute > 0) {
                    _this.second = 59;
                    _this.minute--;
                }
            }
            _this.domElement.innerHTML = _this.createText();
        }
        _this.tiktak++;
        if (_this.second <= 0 && _this.minute <= 0) _this.finish();
    };

    this.finish = function () {
        if(_this.debug) console.log('timer.finish');
        clearInterval(_this.Interval);
        _this.callback();
    };

    this.pause = function () {
        if(_this.debug) console.log('timer.pause');
        clearInterval(_this.Interval);
        delete _this.Interval;
    };

    this._reset = function () {
        if(_this.debug) console.log('timer._reset');
        clearInterval(_this.Interval);
        delete _this.Interval;
        delete _this.second;
        delete _this.minute;
        _this.tiktak = 0;
    };
};