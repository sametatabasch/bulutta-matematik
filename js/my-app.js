var debug = false;
/**
 * dom7 sınıfına her yerden kolayca ulaşabilmek için $$ kullanılacak
 */
window.$$ = Dom7;

// Initialize your app
var game = new game();
var doubleBack = false;

/* Google Admob with cordova-plugin-admobpro */
// select the right Ad Id according to platform
var admobid = {};
if (/(android)/i.test(navigator.userAgent)) { // for android & amazon-fireos
    admobid = {
        banner: 'ca-app-pub-2683501648074485/7289695457', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2683501648074485/7150094658'
    };
} else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) { // for ios
    admobid = {
        banner: 'ca-app-pub-xxx/zzz', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-xxx/kkk'
    };
} else { // for windows phone
    admobid = {
        banner: 'ca-app-pub-xxx/zzz', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-xxx/kkk'
    };
}

function onDeviceReady() {
    if (debug) console.log('DeviceReady');
    /**
     * framework7, question, timer, game.leaderBoard gibi sınıflara erişim sağlar ve oyunu çalışmaya hazırlar
     */
    game.prepare();
    if (debug) console.log('After game prepare');

    /**
     * Create Ad banner on bottom
     */
    if (AdMob) AdMob.createBanner({
        adId: admobid.banner,
        adSize: 'SMART_BANNER',
        overlap: false,
        position: AdMob.AD_POSITION.BOTTOM_CENTER,
        autoShow: false
        // isTesting: true
    });
    if (debug) console.log('After Admob Create banner');
    /**
     * index sayfası yüklendiğinde yapılacak işlemler
     */
    $$(document).on('pageInit pageReinit', '.page[data-page="index"]', function (e) {
        if (debug) console.log("index init");
        game.currentPageName = e.detail.page.name;
        game.isRuning = false;

        //lider tablosu bilgileri yenileniyor
        game.leaderBoard.refresh();

        /*
         * Yeniden başlat butonu gizliyoruz
         */
        $$('#restartLink').hide();

        /**
         * seviye seçimi  için kullanıllar picker
         */
        var pickerDescribe = game.framework7.picker({
            input: '#picker-describe',
            rotateEffect: true,
            toolbarCloseText: 'Seç',
            cols: [
                {
                    values: ('1. Seviye * 2. Seviye * 3. Seviye * 4. Seviye').split('*')
                }
            ]
        });
        // ilk olarak seviyeyi alıyoruz
        game.getLevel('input[name="level"]');


        $$('#startButton').click(function (e) {
            if (debug) console.log('#startButton clicked');
            //eğer seviye seçilmemişse uyarıyoruz

            if (game.level == 0) {
                game.framework7.alert('Seviye seçmelisiniz', 'Dikkat');
                if (debug) console.log('seviye seçilmedi uyarısı verildi');
            } else {
                // oyun sayfasına yönlendiriyoruz.
                game.mainView.router.reloadPage('pageGame.html');
            }
        });
        if (navigator.connection.type !== 'none') AdMob.showBanner(AdMob.AD_POSITION.BOTTOM_CENTER);
    });

    /**
     * Oyun başladı
     */
    $$(document).on('pageInit pageReinit', '.page[data-page="game"]', function (e) {
        game.currentPageName = e.detail.page.name;
        AdMob.hideBanner();//Hide Ad when game playing
        game.question.prepare();
        game.isRuning = true;
        game.timer.set({
            minute: 1,
            second: 0,
            callback: function () {
                game.mainView.router.reloadPage('pageGameOver.html');
            }
        });
        /*
         * Yeniden başlat butonu gösteriyoruz
         */
        $$('#restartLink').show();
        //oyunu başlatıyoruz
        game.start();
    });

    // todo lider tablosundan geri gelince çalışması gerek fakat çalışmıyor.
    $$(document).on('pageBack', '.page[data-page="game"]', function (e) {
        if (debug) console.log('back to game page');
        game.timer.start();

    });

    /**
     * Game over
     */
    $$(document).on('pageInit pageReinit', '.page[data-page="gameOver"]', function (e) {
        if (debug) console.log('gameOver page init');
        game.currentPageName = e.detail.page.name;
        if (navigator.connection.type !== 'none') {
            /**
             * prepare fullscreen Ad at beginning
             * this ad shows one time in 3 minute
             */
            AdMob.prepareInterstitial({
                adId: admobid.interstitial,
                autoShow: false
            });
        }
        game.gameOver();


        $$('#restartButton').click(function () {
            if (debug) console.log('#restartButton clicked');
            game.reStart();
        });
        if (navigator.connection.type !== 'none')
            AdMob.showBanner(AdMob.AD_POSITION.BOTTOM_CENTER);
    });

    $$(document).on('pageBack', '.page[data-page="gameOver"]', function () {
        if (debug) console.log("back to game over");
        game.currentPageName = e.detail.page.name;
        $$('#positionInTheRanking').html(game.leaderBoard.showGameScore());
    });
    /**
     * Lider Tablosu
     */
    $$(document).on('pageInit pageReinit', '.page[data-page="leaderBoard"]', function (e) {
        if (debug) console.log("leaderBoard init");
        game.currentPageName = e.detail.page.name;
        /*
         * Yeniden başlat butonu gizliyoruz
         */
        $$('#restartLink').hide();
        if (game.isRuning) {
            /**
             * Panel kapanma animasyonu başlangıcında timer.start fonksiyonu çalıştırıldığı için
             * buraya eklenen timer.stop fonksiyonu işlevsiz oluyor bu yüzden panel akapatma animasyonu
             * sonunda timer.stop fonksiyonu çağırılıyor
             */
            $$('.panel-right').on('closed', function () {
                if (debug) console.log('popup or right panel closed');
                game.timer.pause();
            });
        }
        game.isRuning = false;
        game.leaderBoard.refresh();
        //lider tablosu bilgileri yenileniyor
        $$('#leaderBoardList').html(game.leaderBoard.show());
        $$('#clearLeaderBoard').click(function () {
            game.framework7.confirm('Lider tablosunu silmek üzeresiniz.Bu işlem geri alınamaz.<br> Onaylıyor musunuz?', 'Dikkat',
                function () {
                    game.leaderBoard._reset();
                    $$('#leaderBoardList').html(game.leaderBoard.show());
                });

        });
    });

    /**
     * dış html dosyası kullanılarak popup açmak için
     */
    $$('a[data-popup-url]').click(function () {
        var popupClass = $$(this).attr('data-popup');
        var url = $$(this).attr('data-popup-url');
        $$.get(url, function (data) {
            $$(popupClass).html(data);
            game.framework7.popup(popupClass);
        });
    });

    $$('.popup, .panel-right').on('open', function () {
        if (debug) console.log('popup or panel-right open');
        $$('span.version').html('v'+game.version); // bilgi sayfasında bulunan versiyon bilgisini yazdırıyoruz.
        game.pause();
    });

    $$('.popup, .panel-right').on('close', function () {
        if (debug) console.log('popup or right panel closed');
        game.resume();
    });

    $$('.popup').on('opened', function () {
        game.framework7.isPopupOpened = true;
        game.framework7.openedPopup = $$(this).attr('data-popup');
    });

    /**
     * linklerin açılması için cordova whitelist eklentisi kullanrak window.open() fonksiyonu çalışyırılacak
     */
    $$('a.external').click(function (e) {
        e.preventDefault();
        var href = $$(this).attr('href');
        if (href !== '#' && href !== '' && href !== 'javascript:') window.open(href);
    });

    /**
     * Kapatma butonuna basıldığında uygulamanın kapanması için
     */
    $$('#cancelLink').click(function () {
        game.framework7.confirm('Çıkmak istediğinizden emin misiniz?',
            function () {
                //Click Cancel
                navigator.app.exitApp();
            },
            function () {
                //Click OK
            }
        );
    });
    /**
     * Oyunu sonlandırıp anasayfaya döndürüren buton
     */
    $$('#restartLink').click(function () {
        /**
         * Panel kapanma animasyonu başlangıcında timer.start fonksiyonu çalıştırıldığı için
         * buraya eklenen timer.stop fonksiyonu işlevsiz oluyor bu yüzden panel akapatma animasyonu
         * sonunda timer.stop fonksiyonu çağırılıyor
         */
        $$('.panel-right').on('closed', function () {
            if (debug) console.log('popup or right panel closed');
            game.pause();
        });
        game.framework7.confirm('Bu oyunu sonlandırıp ana sayfaya dönmek istediğinizden emin misiniz?',
            function () {
                // Click OK
                game.reStart();
            },
            function () {
                //Click Cancel
                game.resume();
            }
        )
    });
    /**
     * Android uygulamada uygulamadan home tuşuna basılıp çıkıldığında uygulanacak işlemler.
     */
    document.addEventListener("pause", function () {
        //console.log("Pause");
        game.pause();
    }, false);
    /**
     * Uygulamaya geri dönüldüğünde yapılacak işlemler
     */
    document.addEventListener("resume", function () {
        //console.log("Resume");
        game.resume();
    }, false);
    /**
     * Cihazda seçenek düğmesine basıldığında sağ menünün açılmasını sağlar
     */
    document.addEventListener("menubutton", function (e) {
        e.preventDefault();

        if ($$('body').hasClass('with-panel-right-cover')) {
            game.framework7.closePanel()
        } else {
            game.framework7.openPanel('right');
        }
    }, false);
    /**
     * Cihazda geri butonuna basıldığında yapılacak işlemler
     */
    document.addEventListener("backbutton", function (e) {
        e.preventDefault();
        // çift tıklema ile uygulamadan çıkış için
        if ($$('.modal').length >= 1) {// birden fazla diyalog açılmasını engellemek için
            game.framework7.closeModal();
        }
        if (doubleBack) {
            game.framework7.confirm('Çıkmak istediğinizden Emin misiniz?',
                function () {
                    //Click OK
                    navigator.app.exitApp();
                },
                function () {
                    //Click Cancel
                }
            );
            return;
        }
        else {
            game.framework7.addNotification({
                message: "Oyundan çıkmak için tekrar geri tıklayınız",
                button: {},
                hold: 2500
            });


            doubleBack = true;
            setInterval(function () {
                doubleBack = false;
            }, 2000);
            // /çift tıklama
            /*
             *  Popup ve modal kontrolleri
             */
            // İf panel opened then close panel
            if ($$('body').hasClass('with-panel-right-cover')) {
                game.framework7.closePanel();
                return;
            }
            // if tehere is opened popup then close it
            if (game.framework7.isPopupOpened) {
                game.framework7.closeModal(game.framework7.openedPopup);
                return;
            }
            /*
             *  sayfalara öze kontroller
             */
            // oyun sayfası
            if (game.currentPageName === 'game') {
                // yeniden başla butonu tetikleniyor
                $$('#restartLink').trigger('click');
                return;
            }
            // Liderler tablosu sayfası
            if (game.currentPageName === 'leaderBoard') {
                game.mainView.router.back();
                return;
            }
            // oyun bitti sayfası
            if (game.currentPageName === 'gameOver') {
                game.reStart();
                return;
            }
            if(game.currentPageName === 'index'){
                game.framework7.confirm('Çıkmak istediğinizden Emin misiniz?',
                    function () {
                        //Click OK
                        navigator.app.exitApp();
                    },
                    function () {
                        //Click Cancel
                    }
                );
            }
        }
    }, false);
    document.addEventListener("offline", function () {
        console.log('Offline oldu')
    }, false);
    document.addEventListener("online", function () {
        console.log('Online oldu')
    }, false);
}
/**
 * Cordova API tam olarak yüklendikten sonra diğer olay dinleyiciler aktif ediliyor
 */
document.addEventListener('deviceready', onDeviceReady, false);