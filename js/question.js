/**
 * Created by sametatabasch on 05.10.2015.
 */
var question = function () {
    'use strict';
    var _this = this;
    this.debug = false;
    this.parent = {};
    /**
     * Doğru cevap
     * @type {number}
     */
    this.result = 0;
    /**
     * işlem
     * @type {string}
     */
    this.operation = '';
    /**
     * sorulan sonru sayısını tutar. Oyun sonunda gösterilen fakat cevaplanamadan kaybolan soruyu düşmek için -1 değerinde başlattılıyor
     * @type {number}
     * @private
     */
    this._count = -1;
    /**
     * verilen yanlış cevap sayısı.
     * bu sayıya göre sorudan alınacak puan belirlenecek
     * @type {number}
     */
    this.wrongAnswerCount = 0;
    /**
     * sonuç kontrol edilirken tıklanan balonun id numarasını tutar
     * @type {string}
     */
    this.clickedBalloonId = '';
    this.balloons = {};
    this.media = {};
    this.exEffect = new explodeEffect();

    /**
     *  add score for each correct
     * @type {number[]}
     */
    this.addPoint = [10, 7, 3, 0];
    /**
     * gameQuestion nesnesinin çağırılmasının my-app.js başında yapılabilmesi için oluşturulan fonksiyon
     * bu fonksiyon sayesinde oyun sırasında oyun ekranından çıkıp tekrar gelindiğinde otun bilgileri sıfırlanmıyor.
     * ve Media nesnesi device ready içerisinde çağırıldığı için sorun olmuyor.
     */
    this.prepare = function () {
        _this.media = new Media('/android_asset/www/correct.mp3',
            // success callback
            function () {
                //if (_this.debug) console.log("playAudio():Audio Success");
            },
            // error callback
            function (err) {
                //if (_this.debug) console.log("playAudio():Audio Error: " + err);
            });
    };

    /**
     * Belirtilen aralıkta sayı üretir
     * first ve last dahil
     * @param first
     * @param last
     * @returns {number}
     * @private
     */
    this._random = function (first, last) {
        if (_this.debug) console.log('question._random');
        return Math.floor(Math.random() * ((last + 1) - first) + first);
    };

    /**
     * Yeni soru üretirken kullanılacak işlemi belirler
     * @returns {*}
     */
    this.setOperation = function () {
        if (_this.debug) console.log('question.setOperation');
        var arr = [];
        switch (_this.parent.level) {
            case 1 :
                arr = ['+', '+', '-', '+', '+', '-', '+', '+', '-', '-'];
                _this.operation = arr[_this._random(0, 9)];
                break;
            case 2 :
                arr = ['+', '+', '-', '+', '+', '-', '+', '+', '-', '-'];
                _this.operation = arr[_this._random(0, 9)];
                break;
            case 3 :
                arr = ['+', '/', '-', '*', '+', '-', '*', '+', '/', '-'];
                _this.operation = arr[_this._random(0, 9)];
                break;
            case 4 :
                arr = ['+', '/', '-', '*', '+', '-', '*', '+', '/', '-'];
                _this.operation = arr[_this._random(0, 9)];
                break;
            default :
                arr = ['+', '+', '-', '+', '+', '-', '+', '+', '-', '-'];
                _this.operation = arr[_this._random(0, 9)];
                break;
        }
    };
    /**
     * girilen diziyi karıştırır ve farklı sıralanmış yeni diziyi döndürür
     * @link http://jsfromhell.com/array/shuffle
     * @param arr
     * @returns {*}
     */
    this.shuffle = function (arr) {
        if (_this.debug) console.log('question.shuffle');
        for (var j, x, i = arr.length; i; j = parseInt(Math.random() * i),
            x = arr[--i], arr[i] = arr[j], arr[j] = x
        );
        return arr;
    };

    /**
     * Bölme işleminde kalansız sonuç çıkması için Bölen değeri hesaplar
     */
    this.adjustDivisions = function (firstNumber, secondNumber) {
        if (_this.debug) console.log('bölme işlemi için ayarlama yapılacak');
        /**
         * 1-30 arası asal sayılar. Her seferinde hesaplatmak yerine direk dizi olarak tutmak daha iyi
         * @type {number[]}
         */
        var primeNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29];
        /**
         * Bölme işlemli sorularda bölüm değerini tutan değişken
         */
        var quotient;
        if (_this.operation === '/' && firstNumber % secondNumber > 0) {
            if (_this.debug) console.log('sayılar kalansız bölünemiyor işlem yapılacak');
            /**
             * Asal olmayan sayının tam bölenlerini tutan değişken
             * @type {Array}
             */
            var divisor = [];
            /**
             * eğer sayı asal sayı değilse Bölen kendinden küçük bölenlerinden rasgele olarak seçilir
             */
            if (primeNumbers.indexOf(firstNumber) === -1) {
                for (var i = firstNumber - 1; i > 1; i--) {
                    if (firstNumber % i === 0) {
                        divisor.push(i);
                    }
                }
                secondNumber = divisor[_this._random(0, divisor.length - 1)];
                if (_this.debug) console.log('asal olmayan birinci sayı için ikinci sayı belirlendi. birinci sayı='+firstNumber+' ikinci sayı= '+secondNumber);
            } else {
                quotient = Math.floor(firstNumber / secondNumber);
                firstNumber = secondNumber * quotient;
                if (_this.debug) console.log('asal olan birinci sayı için yeniden atama yapıldı. birinci sayı='+firstNumber+' ikinci sayı= '+secondNumber);
            }
            if (_this.debug) console.log('bölme işlemi için sayılar hesaplandı');
            return {'firstNumber': firstNumber, 'secondNumber': secondNumber};
        }else{
            if (_this.debug) console.log('sayılar zaten kalansız bölünüyor'+firstNumber+' / '+secondNumber);
            return {'firstNumber': firstNumber, 'secondNumber': secondNumber};
        }
    };

    /**
     * Doğru cevap dışındaki şıkları oluşturmak için doğru şıkka eklenecek rasgele degeri üretir.
     * @returns {Object}
     */
    this.addRandom = function () {
        if (_this.debug) console.log('question.createQuestion.addRandom');
        var operationArray = ['+', '-'];

        /**
         * Doğru şıkka eklenecek sayının işaretini belirler
         * @returns {string}
         */
        function setOperation() {
            if (_this.debug) console.log('question.createQuestion.setOperation');
            return operationArray[_this._random(0, 1)];
        }

        /**
         * yeni şık için yapılacak işlem
         * @type {string}
         */
        var operation = '';
        /**
         * Yeni şıkka eklenecek değer
         * @type {number}
         */
        var rnd = 0;
        if (_this.result < 4) {
            operation = '+';
            rnd = _this._random(1, 6);
        } else {
            operation = setOperation();
            rnd = _this._random(1, _this.result);
        }

        return eval(_this.result.toString() + operation + rnd.toString());
    };
    /**
     * Cevap seçeneklerini oluşturur
     * @returns Array
     */
    this.setOptions = function () {
        /**
         * Soruda olacak şıkların tutulduğu dizi
         * @type {Array}
         */
        var options = [];
        options.push(_this.result); // seçenekler arasına doşru cevap ekleniyor
        var i = 1;
        // geriye kalan 3 seçenek ekleniyor.
        while (i <= 3) {
            var newOption = _this.addRandom();
            if (options.indexOf(newOption) < 0) {
                i++;
                options.push(newOption);
            }
        }
        if (_this.debug) console.log('seçenekler oluşturuldu');
        //şıkları karıştırıyoruz
        return _this.shuffle(options);
    };

    /**
     * Yeni soru üretir
     * @returns {{firstNumber: string, secondNumber: string, operation: string, result: string, options: Array}}
     */
    this.createQuestion = function () {
        if (_this.debug) console.log('question.createQuestion');
        var firstNumber = 0;
        var secondNumber = 0;
        /**
         * büyük sayı ile küçük sayı yer değiştirilirken kullanılan geçici değişken
         * @type {number}
         */
        var tmp = 0;
        _this.setOperation();
        if (_this.debug) console.log('işlem belirlendi -> '+_this.operation);
        switch (_this.parent.level) {
            case 1:
                firstNumber = _this._random(1, 9);
                secondNumber = _this._random(1, 10-firstNumber);
                // first level don't allow negative and big number always on top
                if (firstNumber < secondNumber) {
                    tmp = firstNumber;
                    firstNumber = secondNumber;
                    secondNumber = tmp;
                }
                if (_this.debug) console.log('birinci seviye sayılar belirlendi');
                break;
            case 2:
                firstNumber = _this._random(1, 30);
                secondNumber = _this._random(1, 30);
                if (firstNumber < secondNumber) {
                    tmp = firstNumber;
                    firstNumber = secondNumber;
                    secondNumber = tmp;
                }
                if (_this.debug) console.log('ikinci seviye sayılar belirlendi');
                break;
            case 3:
                firstNumber = _this._random(1, 10);
                secondNumber = _this._random(1, 10);

                if (firstNumber < secondNumber) {
                    tmp = firstNumber;
                    firstNumber = secondNumber;
                    secondNumber = tmp;
                }
                if (_this.operation === '/') {
                    var newNumbers = _this.adjustDivisions(firstNumber, secondNumber);
                    firstNumber = newNumbers.firstNumber;
                    secondNumber = newNumbers.secondNumber;
                }
                if (_this.debug) console.log('üçüncü seviye sayılar belirlendi');
                break;
            case 4:
                firstNumber = _this._random(1, 30);
                secondNumber = _this._random(1, 10);
                if (firstNumber < secondNumber) {
                    tmp = firstNumber;
                    firstNumber = secondNumber;
                    secondNumber = tmp;
                }
                if (_this.operation === '/') {
                    newNumbers = _this.adjustDivisions(firstNumber, secondNumber);
                    firstNumber = newNumbers.firstNumber;
                    secondNumber = newNumbers.secondNumber;
                }
                if (_this.debug) console.log('dördüncü seviye sayılar belirlendi');
                break;
        }
        _this.result = eval(firstNumber.toString() + _this.operation + secondNumber.toString());
        _this._count++;
        return {
            'firstNumber': firstNumber.toString(),
            'secondNumber': secondNumber.toString(),
            'operation': _this.operation.toString(),
            'result': _this.result.toString(),
            'options': _this.setOptions()
        };
    };

    this.showNewQuestion = function () {
        if (_this.debug) console.log('question.showQuestion');
        /**
         *
         * @type {{firstNumber, secondNumber, operation, result, options}}
         */
        var questionVariables = _this.createQuestion();
        /**
         * Print firstNumber, SecondNumber and operation
         */
        for (var variable in questionVariables) {
            if (questionVariables.operation === '/') {
                $$('.question').addClass('divide');
            } else {
                if ($$('.question').hasClass('divide')) $$('.question').removeClass('divide');
            }
            $$('.' + variable).html(questionVariables[variable]);
        }
        if (_this.debug) console.log('işlem yazıldı');
        /**
         * print balloons
         */
        for (var i = 1; i <= 4; i++) {
            var ballonDiv = $$('#balloon' + i.toString());
            if (ballonDiv.find('.balloon').length === 0) {
                ballonDiv.append('<div class="balloon"></div>');
            }
        }
        if (_this.debug) console.log('balonlar eklendi');
        /**
         * Print options value
         */
        _this.balloons = $$('.balloon');
        for (var j = 0; j <= 3; j++) {
            $$(_this.balloons[j]).html('<div class="content"><span>' + questionVariables.options[j] + '</span></div>');
        }
        _this.exEffect.clip();// patlama efekti için balon parçaları oluşturuluyor
        _this.wrongAnswerCount = 0; // her yeni soruda yanlış cevap sayısı sıfırlanıyor
        _this.clickedBalloonId = '';
        $$('.balloon div').click(function () {
            _this.checkanswer($$(this).parent('.balloon'));
        });
        if (_this.debug) console.log('balonlara seçenekler yazıldı');
    };

    this.checkanswer = function (clikedBalloon) {
        if (_this.debug) console.log('question.checkanswer');
        // aynı balona tıklanıp tıklanmadığını kontrol eder. aynı balon ise fonksiyonu çalıştırmaz
        if (_this.clickedBalloonId == $$(clikedBalloon).parent('.balloonDiv').attr('id')) {
            if (_this.debug) console.log('Aynı balona tıklandı');
            return;
        } else {
            if (_this.debug) console.log('yeni balona tıklandı');
            _this.clickedBalloonId = $$(clikedBalloon).parent('.balloonDiv').attr('id');
        }
        var optionValue = $$(clikedBalloon).find('span').html();
        if (optionValue === _this.result) {
            if (_this.debug) console.log('correct answer');
            _this.media.play();//play correct sound
            _this.parent.score += _this.addPoint[_this.wrongAnswerCount] * _this.parent.level;
            if (_this.debug) console.log('puan verildi');
            //show feedback
            _this.parent.timer.pause();
            $$('<div class="feedback"> + ' + _this.addPoint[_this.wrongAnswerCount] * _this.parent.level + '</div>').insertAfter('.page[data-page="game"]');
            //document.addEventListener('afterExplodeEffectTrue', _this.correctAnswerHandle);
            setTimeout(function () {
                $$('.feedback').remove();
                $$('span#score').html(_this.parent.score);
                _this.correctAnswerHandle(); // doğru balon patlama sonrası çalıştırılan fonksiyon. patlama efektini kaldırdığım için manuel çağırıyorum
            }, 500);
            $$(clikedBalloon).remove();// patlama efekti kaldığıldığı için tıklanan balon manuel olarak siliniyor
            //_this.exEffect.explode(clikedBalloon, true);

        } else {
            if (_this.debug) console.log('wrong answer');
            navigator.vibrate(500);
            _this.wrongAnswerCount++;
            //document.addEventListener('afterExplodeEffectFalse', _this.wrongAnswerHandle);
            //_this.exEffect.explode(clikedBalloon, false);
            $$(clikedBalloon).remove();// patlama efekti kaldığıldığı için tıklanan balon manuel olarak siliniyor
            _this.wrongAnswerHandle();// yanlış balon patlama efekti sonrası çalıştırılacak fonksiyon. patlama efekti kaldırıldığı için manuel çağıriyorum
        }
    };
    /**
     * doğru balon tıklandığında ve patlama efekti sona erdiğnde yapılacak işlermler
     */
    this.correctAnswerHandle = function () {
        if (_this.debug) console.log('question.correctAnswerHandle');
        _this.balloons.remove();
        _this.showNewQuestion();
        _this.parent.timer.start();
        //document.removeEventListener('afterExplodeEffectTrue', _this.correctAnswerHandle);
    };
    /**
     * yanlış balon tılandığında ve patalama efekti sona erdiğinde yapılacak işlemler
     */
    this.wrongAnswerHandle = function () {
        if (_this.debug) console.log('question.wrongAnswerHandle');
        //document.removeEventListener('afterExplodeEffectFalse', _this.wrongAnswerHandle);
    };

    this._reset = function () {
        if (_this.debug) console.log('question._reset');
        _this._count = 0;
        _this.parent.score = 0;
        _this.result = 0;
        $$('span#score').html(_this.parent.score);
    };
};