/**
 * Created by sametatabasch on 25.07.2016.
 */
var game = function () {
    /**
     *
     * @type {game}
     */
    var _this = this;
    /**
     * Versiyon bilgisi
     * @type {string}
     */
    this.version = "1.4.8";
    this.debug = false;
    /**
     * Oyun zamanlayıcısı
     * @type {timer}
     */
    this.timer = new timer();
    /**
     * Soru nesnesi
     * @type {question}
     */
    this.question = new question();
    /**
     * leaderBoard nesnesi. Oyun lider tablosu işlemlerini kontrol eder
     * @type {leaderBoard}
     */
    this.leaderBoard = new leaderBoard();
    /**
     * Oyun skoru
     * @type {number}
     */
    this.score = 0;
    /**
     * menü açılıp kapatıldığında mevcut sayfanın init kodları çalıştırılıyor
     * bu yüzden gameover sayfasında yeniden skor alınmış gibi işlem yapılıyor.
     * Bu durumun önüne geçmek için isNewScore değişkeni her game sayfası çağırıldığında true yapılacak
     * ve skor kaydedilme işleminden sonra false yapılacak
     * @type {boolean}
     */
    this.isNewScore = false;
    /**
     * Oyun seviyesi
     * @type {number}
     */
    this.level = 0;
    /**
     * oyun ekranının ve oyun zamanlayıcısının aktifliğini belirtir.
     * @type {boolean}
     */
    this.isRuning = false;
    /**
     * Framework7 ye ulaşmak için kullanılacak
     * @type {Window.Framework7}
     */
    this.framework7 = {};
    /**
     * framework 7 sayfalarını yönetecek ana görünüm
     * @type {{}}
     */
    this.mainView = {};
    /**
     * get current page name
     * @type {string}
     */
    this.currentPageName = '';


    this.prepare = function () {
        _this.question.parent = _this;
        _this.timer.parent = _this;
        _this.leaderBoard.parent = _this;

        _this.framework7 = new Framework7({
            template7Pages: true,
            swipePanel: 'right',
            modalTitle: 'Bulutta Matematik',
            modalButtonOk: 'Tamam',
            modalButtonCancel: 'Vazgeç',
            modalPreloaderTitle: 'Yükleniyor...',
            material: true
        });


        _this.mainView = this.framework7.addView('.view-main', {
            domCache: true //enable inline pages
        });

        /*
         * yeniden başlat butonu Oyun dışında gizleniyor
         */
        $$('#restartLink').hide();
        if (_this.debug) console.log("restartLink is hide");

        /*
         * Giriş sayfası yükleniyor
         */
        _this.mainView.router.loadPage('pageIndex.html');

    };

    /**
     * seçici ile belirtilmiş nesneden seviye bilgisinin alır ve game.level değişkenine aktarır.
     * @param selector
     */
    this.getLevel = function (selector) {
        selector = selector || 'input[name="level"]';

        $$('input[name="level"]').on('change', function () {
            if (_this.debug) console.log('level changed');
            _this.level = parseInt($$(selector).val().split('.', 1).toString().trim());
        });
    };

    this.start = function () {

        _this.timer.start();
        _this.question.showNewQuestion();
        _this.isNewScore = true;
    };
    /**
     * oyun durdurulduğunda yapılacak işlemler
     */
    this.pause = function () {
        if (_this.isRuning) _this.timer.pause();
    };
    /**
     * oyun durdurulduktan sonra tekrar başlatıldığında yapılacak işlemler
     */
    this.resume = function () {
        _this.timer.start();
    };
    /**
     * oyun sona erdiğinde yapılacak işlermler
     */
    this.gameOver = function () {
        if (_this.debug) console.log('game gameOver()');
        _this.isRuning = false;
        //lider tablosu bilgileri yenileniyor
        _this.leaderBoard.refresh();
        /*
         * Yeniden başlat butonu Oyun dışında gizleniyor
         */
        $$('#restartLink').hide();
        /*
         * Eğer liderler tablosu dolu ise ve kullanıcının aldığı puan tablonun en düşük puanından büyük ise
         * Sonuç liderler tablosuna yazılabilir
         */
        if ((_this.leaderBoard._lenght < _this.leaderBoard.limit || _this.leaderBoard.minScore <= _this.score) && _this.isNewScore) {
            if (_this.debug) console.log('lidertablosuna girdi');
            var promt = _this.framework7.prompt(_this.question._count + ' Sorudan ' + _this.score + ' puan alarak liderler tablosuna girmeyi hakettiniz.<br>' +
                'Liderler tablosunda gözükmesi için bir isim giriniz:', 'Tebrikler',
                function (value) {
                    // Clicked OK
                    //todo boş veri girildiğinde pencere tekrar gelsin
                    //if (value === null || value === '' || typeof value !== 'string') return;
                    _this.leaderBoard.set(value, _this.score, _this.level);
                    $$('#positionInTheRanking').html(_this.leaderBoard.showGameScore());
                    _this.isNewScore = false;
                },
                function (value) {
                    //Clicked Cancel
                    $$('#positionInTheRanking').html(_this.leaderBoard.showGameScore());
                    _this.isNewScore = false;
                }
            );
            window.addEventListener('native.keyboardshow', keyboardShowHandler);
            window.addEventListener('native.keyboardhide', keyboardHideHandler);

            var marginTop = parseInt($$('.modal-in').css('margin-top'));
            function keyboardShowHandler(e) {
                var newMarginTop = marginTop - e.keyboardHeight;
                $$('.modal-in').css('margin-top', newMarginTop + 'px');
            }

            function keyboardHideHandler(e) {
                $$('.modal-in').css('margin-top', marginTop + 'px');
            }

            if (localStorage.previousName) {
                $$('input', promt)[0].value = localStorage.previousName;
            } else {
                // focus promt input
                $$('input', promt)[0].focus();
            }
            $$('input', promt).on('keydown', function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    $$('.modal-button-bold', promt).trigger('click')
                }
            });

        } else if (_this.isNewScore) {
            if (_this.debug) console.log('lider tablosuna giremedi');
            _this.framework7.alert(_this.question._count + ' Sorudan ' + _this.score + ' puan aldınız');
            $$('.leaderBoardTitle').html('En yüksek 3 puan');
            $$('#positionInTheRanking').html(_this.leaderBoard.show(3));
            _this.isNewScore = false;
        }
    };
    this.reStart = function () {
        _this.question._reset();
        _this.timer._reset();
        game.level = 0;
        if (navigator.connection.type !== 'none')
        // show full screen ad when clicked reStartButton
            AdMob.showInterstitial();
        _this.mainView.router.reloadPage('pageIndex.html');
    }
};